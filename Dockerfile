# Используем образ Nginx из официального репозитория
FROM nginx:latest

# Копируем наш файл конфигурации nginx.conf внутрь контейнера
COPY nginx.conf /etc/nginx/nginx.conf

# Определяем рабочую директорию для сайта
WORKDIR /usr/share/nginx/html

# Копируем файлы сайта внутрь контейнера
COPY index.html index.html

# Определяем порт, на котором будет работать Nginx
EXPOSE 80

# Запускаем Nginx в режиме демона при запуске контейнера
CMD ["nginx", "-g", "daemon off;"]
